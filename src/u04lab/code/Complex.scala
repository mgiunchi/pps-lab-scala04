package u04lab.code

trait Complex { //operazioni del tipo di dato
  def re: Double // senza (), significa che sono campi cui accedo senza side effect
  def im: Double
  def +(c: Complex): Complex // should implement the sum of two complex numbers..
  def *(c: Complex): Complex // should implement the product of two complex numbers
}
/*
object Complex { //companion object cn factory  (metodo) per restituire un numero complesso (es. apply())
  def apply(re:Double, im:Double):Complex = new ComplexImpl (re,im) // Fill here ??? è un'espressione che posso mett ovunque ->nothing

    private class ComplexImpl(override val re: Double, override val im: Double) extends Complex {
      override def +(c: Complex): Complex = Complex(re+c.re,im+c.im)

      override def *(c: Complex): Complex = Complex(re*c.re-im*c.im,re*c.im+im*c.re)

      override def toString: String = "ComplexImpl("+re+","+im+")"
    }
}*/

object Complex { //companion object cn factory  (metodo) per restituire un numero complesso (es. apply())
  def nuovo(re:Double, im:Double):Complex = new ComplexImpl (re,im) // Fill here ??? è un'espressione che posso mett ovunque ->nothing
  def nuovo2(re:Double, im:Double):Complex = new ComplexImpl2 (re,im)

  private case class ComplexImpl(re: Double, im: Double) extends Complex {
    override def +(c: Complex): Complex = new ComplexImpl(re+c.re,im+c.im)

    override def *(c: Complex): Complex = new ComplexImpl(re*c.re-im*c.im,re*c.im+im*c.re)
  }

  private case class ComplexImpl2(re: Double, im: Double) extends Complex {
    override def +(c: Complex): Complex = new ComplexImpl2(2*re+c.re,2*(im+c.im))

    override def *(c: Complex): Complex = new ComplexImpl2(2*(re*c.re-im*c.im),2*(re*c.im+im*c.re))
  }
}

object TryComplex extends App {
  val a = Array(Complex.nuovo(10,20), Complex.nuovo(1,1), Complex.nuovo(7,0))
  val b = Array(Complex.nuovo2(10,20), Complex.nuovo2(1,1), Complex.nuovo2(7,0))
  val c = a(0)+a(1)+a(2)
  val d = b(0)+b(1)+b(2)
  println(c, c.re, c.im) // (ComplexImpl(18.0,21.0),18.0,21.0)
  println(d, d.re, d.im) // (ComplexImpl(18.0,21.0),18.0,21.0)
  val c2 = a(0)*a(1)
  println(c2, c2.re, c2.im) // (ComplexImpl(-10.0,30.0),-10.0,30.0)
}

/** Hints:
  * - implement Complex with a ComplexImpl class, similar to PersonImpl in slides
  * - check that equality and toString do not work
  * - use a case class ComplexImpl instead, creating objects without the 'new' keyword
  * - check equality and toString now
  */